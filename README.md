# Columnist – Simple and lightweight multi-column design

Columnist achieves a nice, masonry-like, multi-column design without using absolute positioning.
It doesn't move items to different columns, doesn't do any fancy maths,
all it does it fixing the empty space below shorter columns – simple and lightweight!

 - [Demo](https://columnist.avris.it)
 - [How it works?](https://avris.it/projects/columnist)
 - [Source code](https://gitlab.com/Avris/Columnist) 
 - [NPM](https://www.npmjs.com/package/avris-columnist) 

## Installation

You can either install Columnist as a node module:

    $ npm i --save avris-columnist
    
    or
    
    $ yarn add avris-columnist

Use a CDN:

    <script src="https://glcdn.githack.com/Avris/Columnist/raw/v0.2.1/dist/Columnist.min.js"></script>
    
## Usage

### HTML

    <div class="columnist-wall row">
        <div class="columnist-column col-4">
            <div class="card">...</div>
        </div>
        ...
    </div>

Class names are irrelevant. It's important that a column has only one child element!

### JS

    import Columnist from 'avris-columnist';
    
    const columnist = new Columnist(document.querySelector('.columnist-wall'));
    
    columnist.layout();  // apply layout 
    
    columnist.start(100);  // run every 100 ms -- temporary solution, will be removed
    
    columnist.destroy();  // destroy and clean up
    
### CSS

You can add an animation like this:

    <style lang="scss" scoped>
        .columnist-wall > .columnist-column {
            transition: margin-top .2s ease-in-out;
        }
    </style>


## Copyright
 
 * **Author:** Andre Prusinowski [(Avris.it)](https://avris.it)
 * **Licence:** [MIT](https://mit.avris.it)
 