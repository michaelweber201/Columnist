export default class Columnist {
    constructor($container) {
        this.$container = $container;
        this.timer = null;
    }

    layout() {
        const columns = this._countColumns();

        const margins = {};
        let i = 0;
        this.$container.childNodes.forEach($col => {
            const style = this._getStyle($col);
            if (!style) {
                return;
            }

            const extraHeight = parseInt(style.marginBottom.slice(0, -2), 10)
                + parseInt(style.paddingBottom.slice(0, -2), 10)
                + parseInt(style.borderBottomWidth.slice(0, -2), 10);

            margins[i] = Math.max(0, $col.offsetHeight - this._findMainChild($col).offsetHeight - extraHeight);

            if (margins[i - columns] !== undefined) {
                $col.style.marginTop = `-${margins[i - columns]}px`;
            }

            i++;
        });

        return this;
    }

    _countColumns() {
        const columnWidth = this._getColumnWidth();

        return columnWidth ? Math.floor(this.$container.offsetWidth / columnWidth) : 1;
    }

    _getColumnWidth() {
        for (let $col of this.$container.childNodes) {
            if (this._getStyle($col)) {
                return $col.offsetWidth - 1;
            }
        }

        return null;
    }

    _getStyle($el) {
        if (!($el instanceof Element)) {
            return null;
        }
        const style = $el.currentStyle || window.getComputedStyle($el);
        if (style.display === 'none') {
            return null;
        }

        return style;
    }

    _findMainChild($parent) {
        for (let $child of $parent.childNodes) {
            if ($child instanceof Element) {
                return $child;
            }
        }
    }

    start(interval = 300) {
        this.timer = setInterval(() => this.layout(), interval);

        return this;
    }

    destroy() {
        if (this.timer) {
            clearInterval(this.timer);
            this.timer = null;
        }

        this.$container.childNodes.forEach(($col, i) => {
            if (this._getStyle($col)) {
                $col.style.marginTop = null;
            }
        });

        return this;
    }
}
